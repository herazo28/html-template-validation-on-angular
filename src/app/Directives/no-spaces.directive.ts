import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[NoSpaces]'
})
export class NoSpaces {

  constructor(private el: ElementRef) { }

  @Input() NoSpaces: boolean;

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    let e = <KeyboardEvent> event;
    if (this.NoSpaces) {
        // Ensure that it is a number and stop the keypress
        if (e.keyCode == 32) {
            e.preventDefault();
        }
        else {
          return;
        }
      }
  }
}