import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[OnlyLetter]'
})
export class OnlyLetter {

  constructor(private el: ElementRef) { }

  @Input() OnlyLetter: boolean;

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    let e = <KeyboardEvent> event;
    if (this.OnlyLetter) {

        
      if(
        // Letter from a to z
        (e.keyCode >= 65 && e.keyCode <= 90)
        // Backspace key
        || (e.keyCode == 8)
        // Delete key
        || (e.keyCode == 46)
        // Left and right arrow
        || (e.keyCode == 37 || e.keyCode == 39)
        ) {
        return;
      }
      else {
        e.preventDefault();
      }
    }
  }
}