// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// Directives
import { OnlyNumber } from './Directives/only-number.directive';
import { NoSpaces } from './Directives/no-spaces.directive';
import { OnlyLetter } from './Directives/only-letter.directive';
import { Uppercase } from './Directives/uppercase.directive';

@NgModule({
  declarations: [
    AppComponent,
    OnlyNumber,
    NoSpaces,
    OnlyLetter,
    Uppercase
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
